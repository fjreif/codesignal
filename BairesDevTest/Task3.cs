﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BairesDevTest
{
    public class Task3
    {
        /// <summary>
        /// http://www.cut-the-knot.org/pythagoras/DistanceFormula.shtml
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public double ClosestPointPair(int[][] p)
        {
            double minimumDistance = double.MaxValue;
            for (int i = 0; i < p.Length - 1; i++)
            {
                int[] coordenate1 = p[i];
                int[] coordenate2 = p[i+1];
                double eucliden = EuclideanDistance(coordenate1, coordenate2);                
                if (eucliden < minimumDistance)
                {
                    minimumDistance = eucliden;
                }
            }
            return minimumDistance;
        }

        /// <summary>
        /// dist((x, y), (a, b)) = √(x - a)² + (y - b)²
        /// </summary>
        /// <returns></returns>
        double EuclideanDistance(int[] point1, int[] point2)
        {
            double calcA = Math.Pow(point1[0] - point2[0], 2);
            double calcB = Math.Pow(point1[1] - point2[1], 2);
            double dist = Math.Sqrt(calcA + calcB);
            return Math.Round(dist, 9);
        }        

    }
}
