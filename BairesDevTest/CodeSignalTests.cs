﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BairesDevTest
{
    public class CodeSignalTests
    {

        public int CenturyFromYear(int year)
        {
            if (year < 100)
            {
                return 1;
            }
            else if (year > 100)
            {
                int resto;
                int quociente = Math.DivRem(year, 100, out resto);
                if (resto == 0)
                {
                    return quociente;
                } else
                {
                    return quociente + 1;
                }                
            }
            return 0;
        }

        /// <summary>
        /// For inputString = "aabaa", the output should be
        /// checkPalindrome(inputString) = true;
        /// For inputString = "abac", the output should be
        /// checkPalindrome(inputString) = false;
        /// For inputString = "a", the output should be
        /// checkPalindrome(inputString) = true.
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public bool CheckPalindrome(string inputString)
        {
            if (inputString.Length == 1)
            {
                return true;
            }

            string reverse = new string(inputString.Reverse().ToArray());
            if (inputString == reverse)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Given an array of integers, find the pair of adjacent elements that has the largest product and return that product.
        /// For inputArray = [3, 6, -2, -5, 7, 3], the output should be
        /// adjacentElementsProduct(inputArray) = 21.
        /// 7 and 3 produce the largest product.
        /// </summary>
        /// <param name="inputArray"></param>
        /// <returns></returns>
        public int AdjacentElementsProduct(int[] inputArray)
        {
            int biggest = int.MinValue;
            for (int i = 0; i < inputArray.Length; i++)
            {
                if ((i + 1) > (inputArray.Length - 1))
                {
                    continue;
                }
                int first = inputArray[i];
                int second = inputArray[i + 1];
                int result = first * second;
                if (result > biggest)
                {
                    biggest = result;
                }
            }

            return biggest;
        }
        /// <summary>
        /// Below we will define an n-interesting polygon. Your task is to find the area of a polygon for a given n.
        /// A 1-interesting polygon is just a square with a side of length 1. 
        /// An n-interesting polygon is obtained by taking the n - 1-interesting polygon and appending 
        /// 1-interesting polygons to its rim, side by side.You can see the 1-, 2-, 3- and 4-interesting polygons in the picture below.
        /// 
        /// Example
        /// For n = 2, the output should be
        /// shapeArea(n) = 5;
        ///For n = 3, the output should beshapeArea(n) = 13.
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public int ShapeArea(int n)
        {
            return ((n * n) + ((n - 1) * (n - 1)));
        }

        /// <summary>
        /// Ratiorg got statues of different sizes as a present from CodeMaster for his birthday, 
        /// each statue having an non-negative integer size. Since he likes to make things perfect, 
        /// he wants to arrange them from smallest to largest so that each statue will be bigger 
        /// than the previous one exactly by 1. He may need some additional statues to be able to 
        /// accomplish that. Help him figure out the minimum number of additional statues needed.
        /// 
        /// Example
        /// For statues = [6, 2, 3, 8], the output should be
        /// makeArrayConsecutive2(statues) = 3.
        /// Ratiorg needs statues of sizes 4, 5 and 7.
        /// 
        /// Test 2
        ///Input: statues: [0, 3]
        /// Expected Output:2
        /// </summary>
        /// <param name="statues"></param>
        /// <returns></returns>
        public int MakeArrayConsecutive2(int[] statues)
        {
            Array.Sort<int>(statues);
            int totalStatues = 0;
            for (int i = statues.Length - 1; i > 0 ; i--)
            {
                int actual = statues[i];
                int next = statues[i - 1];

                totalStatues += ((actual - next) - 1);

            }
            return totalStatues;
        }

        /// <summary>
        /// https://app.codesignal.com/arcade/intro/level-2/2mxbGwLzvkTCKAJMG
        /// Given a sequence of integers as an array, determine whether it is possible to obtain a strictly increasing sequence by removing no more than one element from the array.
        /// Note: sequence a0, a1, ..., an is considered to be a strictly increasing 
        /// if a0<a1< ... < an.Sequence containing only one element is also considered to be 
        /// strictly increasing.
        /// 
        /// Example
        /// For sequence = [1, 3, 2, 1], the output should be
        /// almostIncreasingSequence(sequence) = false.
        /// 
        /// For sequence = [1, 3, 2], the output should be
        /// almostIncreasingSequence(sequence) = true/// 
        /// There is no one element in this array that can be removed in order to get a 
        /// strictly increasing sequence.
        /// You can remove 3 from the array to get the strictly increasing sequence [1, 2]. 
        /// Alternately, you can remove 2 to get the strictly increasing sequence [1, 3]
        /// 
        /// </summary>
        /// <param name="sequence"></param>
        /// <returns></returns>
        public bool AlmostIncreasingSequence(int[] sequence)
        {
            int total = 0;
            for (int i = 0; i < sequence.Length - 1; i++)
            {
                int actual = sequence[i];
                int next = sequence[i + 1];                
                if (!(actual < next))
                {
                    total++;
                    var numbersList = sequence.ToList();
                    if (i == 0)
                    {
                        numbersList.RemoveRange(i, 1);
                    } else
                    {
                        int previous = sequence[i - 1];
                        if (next <= previous)
                        {
                            numbersList.RemoveRange(i + 1, 1);
                        } else if (next > previous || i == 0)
                        {
                            numbersList.RemoveRange(i, 1);
                        }
                    }
                    sequence = numbersList.ToArray();
                    i -= 2;
                    if (i < -1)
                    {
                        i = -1;
                    }
                } else if (actual == next) {
                    total++;
                }
                
                if (total > 1)
                {
                    return false;
                }
            }
            return true;
        }


    }
}
