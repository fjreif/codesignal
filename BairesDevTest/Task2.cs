﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BairesDevTest
{
    public class Task2
    {

        public int StringsConstruction(string a, string b)
        {
            int total = 0;
            int indexFound = b.IndexOf(a);
            while (indexFound > -1)
            {
                total += 1;
                int start = indexFound + 1;
                if (start > b.Length)
                {
                    start = b.Length - 1;
                }
                string newB = b.Substring(start, b.Length - 1 - start);
                indexFound = newB.IndexOf(a);
            }

            string reverseB = new string(b.Reverse().ToArray());
            indexFound = reverseB.IndexOf(a);
            while (indexFound > -1)
            {
                total += 1;
                string newB = reverseB.Substring(indexFound + 1, reverseB.Length - 1);
                indexFound = newB.IndexOf(a);
            }

            return total;
        }


    }
}
