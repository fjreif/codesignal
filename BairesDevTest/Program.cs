﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BairesDevTest
{
    class Program
    {
        static void Main(string[] args)
        {

            //Task1 task1 = new Task1();
            //bool isValid = task1.ValidBracketSequenceV2("()"); //true
            //isValid = task1.ValidBracketSequenceV2("()[]{}"); //true
            //isValid = task1.ValidBracketSequenceV2("(]"); //false
            //isValid = task1.ValidBracketSequenceV2("([)]"); //false
            //isValid = task1.ValidBracketSequenceV2("{[]}"); //true
            //isValid = task1.ValidBracketSequenceV2("({[]})"); //true

            //Task2 task2 = new Task2();
            //int ret = task2.StringsConstruction("b", "abccba");

            int[][] input = {
                new int[] { 0, 11 }, 
                new int[] { -7, 1 }, 
                new int[] { -5, -3 } 
            }; //4.472135955
            Task3 task3 = new Task3();
            double closetPoint = task3.ClosestPointPair(input);

            //CodeSignalTests codeSignalTests = new CodeSignalTests();
            //int century = codeSignalTests.CenturyFromYear(101);
            //int[] array = new int[] { -23, 4, -3, 8, -12 };
            //int biggest = codeSignalTests.AdjacentElementsProduct(array);

            //int[] array = new int[] { 6, 2, 3, 8 };
            //int totalStatues = codeSignalTests.MakeArrayConsecutive2(array);

            //int[] array = new int[] { 1, 2, 5, 3, 5 }; //true
            //int[] array = new int[] {1, 2, 3, 4, 99, 5, 6 }; //true
            //int[] array = new int[] { 1, 2, 3, 4, 3, 6 }; //true
            //int[] array = new int[] { 10, 2, 3, 4, 6 }; //true
            //bool isIncreaseOnlyOne = codeSignalTests.AlmostIncreasingSequence(array);
        }
    }
}
