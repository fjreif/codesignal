﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BairesDevTest
{
    /// <summary>
    /// 
    /// </summary>
    public class Task1
    {


        /// <summary>
        /// A bracket sequence, consisting of the characters (, ), [, ], {, and }
        /// </summary>
        /// <param name="sequence"></param>
        /// <returns></returns>
        public bool ValidBracketSequence(string sequence)
        {
            if (string.IsNullOrEmpty(sequence))
            {
                return false;
            }
            char[] seq = sequence.ToArray();
            if (seq[0] == ')' || seq[0] == ']' || seq[0] == '}')
            {
                return false;
            }

            int countBracket1Open = 0;
            int countBracket1Close = 0;
            int countBracket2Open = 0;
            int countBracket2Close = 0;
            int countBracket3Open = 0;
            int countBracket3Close = 0;

            for (int i = 0; i < seq.Length; i++)
            {
                char bracket = seq[i];

                if (bracket == '(')
                {
                    countBracket1Open += 1;
                }
                else if (bracket == ')')
                {
                    countBracket1Close += 1;
                }
                else if (bracket == '[')
                {
                    countBracket2Open += 1;
                }
                else if (bracket == ']')
                {
                    countBracket2Close += 1;
                }
                else if (bracket == '{')
                {
                    countBracket3Open += 1;
                }
                else if (bracket == '}')
                {
                    countBracket3Close += 1;
                }              

            }

            if ((countBracket1Open == countBracket1Close) &&
                (countBracket2Open == countBracket2Close) && 
                (countBracket3Open == countBracket3Close))
            {
                return true;
            }

            return false;
        }

        public bool ValidBracketSequenceV2(string sequence)
        {
            string[] openBrackets = new string[] { "(", "[", "{" };
            Hashtable pairs = new Hashtable();
            pairs.Add("(", ")");
            pairs.Add("[", "]");
            pairs.Add("{", "}");
            Stack bracketStack = new Stack();
            if (string.IsNullOrEmpty(sequence))
            {
                return false;
            }
            char[] seq = sequence.ToArray();
            for (int i = 0; i < seq.Length; i++)
            {
                char bracket = seq[i];
                //Check if character is an open bracket
                if (openBrackets.Contains(bracket.ToString()))
                {
                    bracketStack.Push(bracket);
                } else
                {
                    //If statck is empty sequence is not valid
                    if (bracketStack.Count == 0) return false;

                    char topBracket = (char)bracketStack.Peek();
                    string closingBracket = (string)pairs[topBracket.ToString()];
                    if (bracket.ToString() != closingBracket) return false;
                    //Remove the top bracket
                    bracketStack.Pop();
                }
            }

            return true;
        }

    }
}
